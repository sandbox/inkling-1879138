<?php

/**
 * @file
 *   The Alternate Localization Server is a module that enables use of a localization server other
 *   than localize.drupal.org
 * 
 *   See README.txt for more information.
 */

/**
 * Implements hook_system_info_alter()
 * 
 * @note We think that using hook_l10n_server_l10n_update_projects_alter() would work better for
 *   our purposes here. However, I haven't been able to get that to work after two attempts. (Update
 *   19 Sept 2012): I have been able to make hook_l10n_server_l10n_update_projects_alter() work in
 *   module l10nprojlist. This may be worth revisiting.
 */
function l10naltserver_system_info_alter(&$info, $file, $type) {
  if ($type == 'module') {
    $alternate_l10n_update_server = variable_get('l10n_client_server', '');
    $file_download_path = variable_get('l10n_update_download_store', '');
    
    // Get l10n_server. (There may be a better way, but I haven't found it yet.)
    $l10n_server = (substr($alternate_l10n_update_server, 0, 7) == 'http://')
      ? substr($alternate_l10n_update_server, 7)
      : $alternate_l10n_update_server; /// strip off http://
    
    $right_char = substr($alternate_l10n_update_server, -1);
    if ($right_char != '/' && $right_char != '\\') {
      $alternate_l10n_update_server .= '/';
    }
            
    $right_char = substr($file_download_path, -1);
    if ($right_char != '/' && $right_char != '\\') {
      $file_download_path .= '/';
    }
            
    //-- Set the alternate l10n update server --//
    
    // See notes at the top of this file regarding these settings. I chose not to include
    // .../%core/%project/... in the ['l10n path'] because the server dumps updates obtained from
    // localize.drupal.org in one directory. The suggested download directory is 
    // /sites/all/translations, but I pull that from l10n_update_download_store. We
    // want to use these files as a basis for our translations.
    $info['l10n server'] = $l10n_server;
    $info['l10n path'] = $alternate_l10n_update_server . $file_download_path .
        '%project-%release.%language.po';
  }
}

/**
 * Implements hook_disable().
 */
function l10naltserver_disable() {
  _l10naltserver_use_default_server();
}

//-----------------------------------------------------------------------------------------------//

/**
 * Reset localize.drupal.org to get translation files
 * 
 * @todo The return value is probably a bit of code copied from l10n_update_build_projects and not
 *   actually needed here, but that needs to be tested.
 * @return array $projects
 * 
 * Copied and slightly modified from l10n_update_build_projects(). Just had to
 * make sure that l10_path was getting reset.
 */

function _l10naltserver_use_default_server() {
  module_load_include('inc', 'l10n_update', 'l10n_update.project');
  
  //-------- The following is copied and slightly modified from l10n_update_build_projects() -----//
  module_load_include('inc', 'l10n_update');
  // Get all stored projects, including disabled ones
  $current = l10n_update_get_projects(NULL, TRUE);
  // Now get the new project list, just enabled ones
  $projects = l10n_update_project_list();

  // Mark all previous projects as disabled and store new project data
  db_update('l10n_update_project')
    ->fields(array(
      'status' => 0,
    ))
    ->execute();

  $default_server = l10n_update_default_server();

  if (module_exists('update')) {
    $projects_info = update_get_available(TRUE);
  }
  foreach ($projects as $name => $data) {
    if (isset($projects_info[$name]['releases']) && $projects_info[$name]['project_status'] != 'not-fetched') {
      // Find out if a dev version is installed.
      if (preg_match("/^[0-9]+\.x-([0-9]+)\..*-dev$/", $data['info']['version'], $matches)) {
        // Find a suitable release to use as alternative translation.
        foreach ($projects_info[$name]['releases'] as $project_release) {
          // The first release with the same major release number which is not 
          // a dev release is the one. Releases are sorted the most recent first.
          if ($project_release['version_major'] == $matches[1] &&
              (!isset($project_release['version_extra']) || $project_release['version_extra'] != 'dev')) {
            $release = $project_release;
            break;
          }
        }
      }
      elseif ($name == "drupal" || preg_match("/HEAD/", $data['info']['version'], $matches)) {
        // Pick latest available release.
        $release = array_shift($projects_info[$name]['releases']);
      }

      if (!empty($release['version'])) {
        $data['info']['version'] = $release['version'];
      }

      unset($release);
    }

    $data += array(
      'version' => isset($data['info']['version']) ? $data['info']['version'] : '',
      'core' => isset($data['info']['core']) ? $data['info']['core'] : DRUPAL_CORE_COMPATIBILITY,
      // The project can have its own l10n server, we use default if not
    //  'l10n_server' => isset($data['info']['l10n server']) ? $data['info']['l10n server'] : NULL,
      'l10n_server' => NULL,
      // A project can provide the server url to fetch metadata, or the update url (path)
      'l10n_url' => isset($data['info']['l10n url']) ? $data['info']['l10n url'] : NULL,
      'l10n_path' => isset($data['info']['l10n path']) ? $data['info']['l10n path'] : NULL,
      'status' => 1,
    );
    $project = (object) $data;
    // --------- Remark out the following so the default server gets used ---------
    // Unless the project provides a full l10n path (update url), we try to build one
    //if (!isset($project->l10n_path)) {
    //  $server = NULL;
    //  if ($project->l10n_server || $project->l10n_url) {
    //    $server = l10n_update_server($project->l10n_server, $project->l10n_url);
    //  }
    //  else {
        // Use the default server
        $server = l10n_update_server($default_server['name'], $default_server['server_url']);
    //  }
      if ($server) {
        // Build the update path for this project, with project name and release replaced
        $project->l10n_path = l10n_update_build_string($project, $server['update_url']);
      }
    //}
    // Create / update project record
    $update = empty($current[$name]) ? array() : array('name');
    drupal_write_record('l10n_update_project', $project, $update);
    $projects[$name] = $project;
  }
  return $projects;
}
