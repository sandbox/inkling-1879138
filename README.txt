Alternate Localization Server
=============================
The l10n_update README.txt says in part:

    Each project i.e. modules, themes, etc. can define alternative translation  servers
    to retrieve the translation updates from. Include the following definition in the
    projects .info file:

       l10n server = example.com
       l10n url = http://example.com/files/translations/l10n_server.xml

    The download path pattern is normally defined in the above defined xml file.   You
    may override this path by adding a third definition in the .info file:

       l10n path = http://example.com/files/translations/%core/%project/%project-%release.%language.po

This module operates on the premise that we want *all* modules to go to another update server for
translation updates. So we use hook_system_info_alter to update those settings as specified  above.
More specifically, we set 110n server and l10n path. The latter overrides l10n url, so we don't need
to bother with an l10n_server.xml file. When the module is deactivated, we these settings are set to
defaults, using hook_disable().

Installation
-------------
Download and unpack the module in the usual way.

Enable this module. It should enable dependencies (l10n_update, l10n_client, and locale).

A tab labeled "Sharing" will appear at Administration >> Configuration >> Regional and language. 
This has a field to enter the address of the localization server you want to use. It is assumed
this address will have Drupal set up with the l10n_server module installed.

See the l10n_update README.txt for more installation information.

Make sure that .po files are not forbidden in .htaccess files! If .po files are forbidden, this
solution will not work. For reference, see: http://drupal.org/node/1011532.

Localization Server Address Form
----------------------------------
A tab labeled "Sharing" is at Administration >> Configuration >> Regional and language >> Languages.
It has a field to enter the address of the localization server. The code for this is found at
l10n_client.module, function l10n_client_settings_form(). That's the reason for the current
dependency on l10n_client.

The main purpose for the l10n_client is to provide the on-page editor for localization strings. If
this module is ever enabled in an environment that doesn't want the on-page editor to show up,
the functionality of l10n_client_settings_form() will need to be put into this module somehow.